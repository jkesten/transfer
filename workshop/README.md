# workshop

This repo contains the snippets used for the k8s support workshop. 

Set the namespace `support` as default namespace (spares typing):
```
kubectl config set-context --current --namespace=support
```

Get the password for kibana from secret (adapt if needed to access other things):
```
kubectl -n elastic get secret elasticsearch-es-elastic-user -o=jsonpath='{.data.elastic}' | base64 --decode; echo
```

Generate the secret for the sftp server:
```
kubectl create secret generic sftp-client-public-keys -n support --from-file=sftp_client_keys
```

Apply the secrets (use the real pod name) and yes it is really *delete*:
```
kubectl get pods
kubectl delete pod sftp-56d556bf7f-bhlwz
``` 

Login into our enercast docker registry:
```
docker login -u user -p pass registry.enercast.de
```

Build a docker image with tags:
```
docker build -t  registry.enercast.de/support-team/scripts/customer-scripts/hello-world:2021-08-02 .
```

Push an image to our registry:
```
docker push registry.enercast.de/support-team/scripts/customer-scripts/hello-world:2021-08-02
```

"jump" into a running pod:
```
kubectl exec -ti support-deployment-648d98cb8f-bpmnp -- sh
```

.pypirc
```
[distutils]
index-servers =
    gitlab

[gitlab]
repository = https://gitlab.enercast.de/api/v4/projects/97/packages/pypi
username = shell-access-token
password = TOKEN_HERE
```
see also https://gitlab.enercast.de/help/user/packages/pypi_repository/index




